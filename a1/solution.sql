--Find all artist that has letter d in name

SELECT * FROM artists WHERE name LIKE "%d%";

--Find all songs that has a length of less than 230

SELECT * FROM songs WHERE length<230;

--Join the 'albums' and songs tables

SELECT albums.album_title, songs.song_name, songs.length FROM albums
JOIN songs ON albums.id = songs.album_id;

--Join artists and albums tables (albums with letter a in its name)

SELECT * FROM artists
JOIN albums ON albums.artist_id = artists.id WHERE albums.album_title LIKE "%a%";

--Sort the albums in Z-A order (show only first 4 records)

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--Join the albums and songs tables (Sort albums from Z-A)

SELECT * FROM albums
JOIN songs ON albums.id = songs.album_id ORDER BY albums.album_title DESC;