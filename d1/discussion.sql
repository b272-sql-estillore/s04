[1] SECTION Add new records

-- Add 5 artists, 2 albums each, 2 songs per album

-- 5 new artists
INSERT INTO artists (name) VALUES
("Taylor Swift"),
("Lady Gaga"),
("Justin Bieber"),
("Ariana Grande"),
("Bruno Mars");


--2 albums for TS
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Fearless", "2008", 3),
("Red", "2012", 3);

--2 songs for Fearless
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Fearless", 402, "Pop rock", 5),
("Love Story", 355, "Country", 5);

--2 songs for Red
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Everything Has Changed", 404, "Country Rock", 6),
("Begin Again", 358, "Country Pop", 6);


--2 albums for Lady Gaga
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("A Star Is Born", "2018-10-05", 4),
("Born This Way", "2011-05-23", 4);

--4 songs for both albums of Lady Gaga
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Shallow", 336, "Pop Country", 7),
("I'll Never Love Again", 529, "Pop Ballad", 7),
("You and I", 507, "Pop rock", 8),
("Marry the Night", 355, "Pop", 8);


--2 albums for Justin Bieber
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Purpose", "2015-11-13", 5),
("Believe", "2012-06-15", 5);

--2 songs for both albums of Justin Bieber
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Love Yourself", 354, "Pop R&B", 9),
("Boyfriend", 252, "Pop", 10);


--2 albums for Ariana Grande
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Dangerous Woman", "2016-05-20", 6),
("thank u, next", "2019-02-08", 6);

--2 songs for both albums of Justin Bieber
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Into You", 405, "Pop R&B", 11),
("thank u, next", 327, "R&B/Soul, Pop", 12);


--2 albums for Bruno Mars
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("24K Magic", "2016-01-01", 7),
("Earth to Mars", "2011-01-01", 7);

--2 songs for both albums of Justin Bieber
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("24K Magic", 400, "Funk, Disco", 13),
("Lost", 250, "R&B/Soul, Pop", 14);


[2] SECTION - Advanced Selection

SELECT * FROM songs WHERE id=11;
SELECT * FROM songs WHERE id!=11;

--Greater than or equal
SELECT * FROM songs WHERE id<11;

--Get specific IDs (OR)
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

SELECT * FROM songs WHERE id IN (11,3,5);
SELECT * FROM songs WHERE genre IN ("Pop", "R&B");

--Combining conditions
SELECT * FROM songs WHERE album_id = 4 AND id < 8;

--Find Partial Matches
SELECT * FROM songs WHERE song_name LIKE "%d";

--Select keyword from the start
SELECT * FROM songs WHERE song_name LIKE "b%"; -- not case sensitive

SELECT * FROM songs WHERE song_name LIKE "%e%";


SELECT * FROM albums WHERE date_released LIKE "2011%";
SELECT * FROM albums WHERE date_released LIKE "20_8-%";


--Sorting Keywords
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

--Getting Distinct records
SELECT DISTINCT genre FROM songs;


[3] Table Joins
--Combine artist table and albums table
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

--More than 2 tables
SELECT * FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

--Select columns to be included in a table
SELECT artists.name, albums.album_title, songs.song_name FROM artists JOIN albums ON artists.id = albums.artist_id JOIN songs ON albums.id = songs.album_id;


--Show artist without records on the right side of the joined table

SELECT * FROM artists
LEFT JOIN albums ON artists.id = albums.artist_id;